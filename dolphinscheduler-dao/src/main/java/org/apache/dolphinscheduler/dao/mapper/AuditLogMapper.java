package org.apache.dolphinscheduler.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.dolphinscheduler.dao.entity.AuditLog;
import org.apache.dolphinscheduler.dao.entity.Resource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * auditlog mapper interface
 */
public interface AuditLogMapper extends BaseMapper<AuditLog> {
    IPage<AuditLog> queryAuditLog(IPage<AuditLog> page);
}
