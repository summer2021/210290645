package org.apache.dolphinscheduler.api.audit;

import org.apache.dolphinscheduler.dao.entity.User;

import java.util.Date;

public class AuditMessage {
    private String messageType;

    private User user;

    private Date auditDate;

    private String module;

    private String operation;

    public AuditMessage(User user, Date auditDate, String module, String operation) {
        this.user = user;
        this.auditDate = auditDate;
        this.module = module;
        this.operation = operation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getAuditDate() {
        return auditDate;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
}
