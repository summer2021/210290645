package org.apache.dolphinscheduler.api.service;

import org.apache.dolphinscheduler.common.enums.ResourceType;
import org.apache.dolphinscheduler.dao.entity.User;

import java.util.Map;

/**
 * audit information service
 */
public interface AuditService {
    void addAudit(User user, String module, String operation);

    Map<String, Object> queryLogListPaging(User loginUser, String searchVal, Integer pageNo, Integer pageSize);
}
