package org.apache.dolphinscheduler.api.audit;

public interface AuditSubscriber {

    /**
     * process the message
     *
     * @param message
     */
    void execute(AuditMessage message);
}
