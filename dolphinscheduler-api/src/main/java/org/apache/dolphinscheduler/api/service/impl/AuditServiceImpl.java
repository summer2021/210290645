package org.apache.dolphinscheduler.api.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dolphinscheduler.api.dto.AuditDto;
import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.service.AuditService;
import org.apache.dolphinscheduler.api.utils.PageInfo;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.utils.CollectionUtils;
import org.apache.dolphinscheduler.dao.entity.AuditLog;
import org.apache.dolphinscheduler.dao.entity.User;
import org.apache.dolphinscheduler.dao.mapper.AuditLogMapper;
import org.apache.dolphinscheduler.dao.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AuditServiceImpl extends BaseServiceImpl implements AuditService {

    @Autowired
    private AuditLogMapper auditLogMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public void addAudit(User user, String module, String operation) {

    }

    @Override
    public Map<String, Object> queryLogListPaging(User loginUser, String searchVal, Integer pageNo, Integer pageSize) {
        HashMap<String, Object> result = new HashMap<>();
        Page<AuditLog> page = new Page<>(pageNo, pageSize);
        IPage<AuditLog> logIPage = auditLogMapper.queryAuditLog(page);
        List<AuditLog> logList = logIPage.getRecords();
        PageInfo<AuditDto> pageInfo = new PageInfo<>(pageNo, pageSize);
        if (CollectionUtils.isEmpty(logList)) {
            pageInfo.setTotalCount(0);
            pageInfo.setLists(new ArrayList<>());
            result.put(Constants.DATA_LIST, pageInfo);
            putMsg(result, Status.SUCCESS);
            return result;
        }
        List<User> userList = userMapper.selectByIds(CollectionUtils.transformToList(logList, AuditLog::getUserId));
        Map<Integer, User> userIdToNameMap = CollectionUtils.collectionToMap(userList, User::getId);
        List<AuditDto> auditDtos = CollectionUtils.transformToList(logList,
                auditLog -> transformAuditLog(auditLog, userIdToNameMap.get(auditLog.getUserId()).getUserName())
        );
        pageInfo.setTotalCount(auditDtos.size());
        pageInfo.setLists(auditDtos);
        result.put(Constants.DATA_LIST, pageInfo);
        putMsg(result, Status.SUCCESS);
        return result;
    }

    private AuditDto transformAuditLog(AuditLog auditLog, String userName) {
        AuditDto auditDto = new AuditDto();
        auditDto.setModule(auditLog.getModule());
        auditDto.setOperation(auditLog.getOperation());
        auditDto.setTime(auditLog.getTime());
        auditDto.setUserName(userName);
        return auditDto;
    }
}
