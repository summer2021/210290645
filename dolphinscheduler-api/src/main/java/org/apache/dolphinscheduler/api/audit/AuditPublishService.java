package org.apache.dolphinscheduler.api.audit;

import org.apache.dolphinscheduler.dao.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Component
public class AuditPublishService {

    /**
     * audit message queue
     */
    private BlockingQueue<AuditMessage> queue = new LinkedBlockingQueue<>(1000);

    /**
     * subscribers list
     */
    @Autowired
    private List<AuditSubscriber> subscribers;

    private static final Logger logger = LoggerFactory.getLogger(AuditPublishService.class);

    /**
     * create a daemon thread to process the message queue
     */
    @PostConstruct
    private void init() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                doPublish(); 
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * publish a new audit message
     *
     * @param message audit message
     */
    public void publish(AuditMessage message) {
        queue.offer(message);
    }

    /**
     *  subscribers execute the message processor method
     */
    private void doPublish() {
        AuditMessage message;
        while (true) {
            try {
                message = queue.take();
                for (AuditSubscriber subscriber : subscribers) {
                    try {
                        subscriber.execute(message);
                    } catch (Exception e) {
                        logger.error("subscriber fail to process message {}", message.getMessageType());
                    }
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     *  generate new Message
     *
     */
    public AuditMessage generateMessage(User user, Date auditDate, String module, String operation) {
        return new AuditMessage(user, auditDate, module, operation);
    }

}
