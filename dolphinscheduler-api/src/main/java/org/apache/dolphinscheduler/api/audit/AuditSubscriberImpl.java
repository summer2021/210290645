package org.apache.dolphinscheduler.api.audit;


import org.apache.dolphinscheduler.dao.entity.AuditLog;
import org.apache.dolphinscheduler.dao.mapper.AuditLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class AuditSubscriberImpl implements AuditSubscriber {

    @Autowired
    AuditLogMapper logMapper;

    @Override
    public void execute(AuditMessage message) {
        AuditLog auditLog = new AuditLog();
        auditLog.setModule(message.getModule());
        auditLog.setOperation(message.getOperation());
        auditLog.setTime(message.getAuditDate());
        logMapper.insert(auditLog);
    }
}
